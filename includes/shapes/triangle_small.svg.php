<svg class="pp-small-triangle" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="currentColor" width="100%" height="<?php echo $height; ?>" viewBox="0 0 0.156661 0.1" role="presentation">
	<polygon points="0.156661,3.93701e-006 0.156661,0.000429134 0.117665,0.05 0.0783307,0.0999961 0.0389961,0.05 -0,0.000429134 -0,3.93701e-006 0.0783307,3.93701e-006 "></polygon>
</svg>